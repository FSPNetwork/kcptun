# Docker-Kcptun

FSP Network Gen2 Server Infrastructure - kcptun client

[![Docker Automated build](https://img.shields.io/docker/automated/fspnetwork/kcptun.svg?style=flat-square)](https://hub.docker.com/r/fspnetwork/kcptun)
[![Docker Build Status](https://img.shields.io/docker/build/fspnetwork/kcptun.svg?style=flat-square)](https://hub.docker.com/r/fspnetwork/kcptun)
![GitHub](https://img.shields.io/github/license/fspnet/docker-kcptun.svg?style=flat-square)

A docker image for [KCPTUN](https://github.com/xtaci/kcptun) client support

### Download from Docker Hub 

    docker pull fspnetwork/kcptun:client

### Usage

    docker run -d --restart=always -e "KCP_REMOTE=host" -e "KCP_REMOTE_PORT=1024" -e "PASSWORD=123456" -p 443:443 -p 443:443/udp --name kcpclient fspnetwork/kcptun:client

### Default configuration in environment variables

| Environment | Default |
| - | - |
| PASSWORD | 123456 |
| KCP_PORT | 443 |
| KCP_REMOTE | host |
| KCP_REMOTE_PORT | 1024 |